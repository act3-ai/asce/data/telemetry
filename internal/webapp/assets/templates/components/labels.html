<h1 id="labels-and-selectors"><a style="text-decoration: none; color: #FFFFFF;" href="#labels-and-selectors">Labels and Selectors</a></h1>

<p>
  Labels are simple key-value pairs that can be added to bottles and parts of
  bottles. We will primarily discuss how labels are used on bottles but they
  apply identically to parts as well. The concept of labels that ACE Data uses
  is the same as Kubernetes
  <a
    href="https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/"
    >labels</a
  >. The syntax for keys and values of labels are restricted in the same way as
  Kubernetes.
</p>

<h2>Labels</h2>

<p>
  Labels are the way that bottle authors can classify and identify sets of bottles. Commonly labels
  are used to specify the:
</p>

<ul>
  <li>
    Project that created it, e.g.
    <span class="badge rounded-pill bg-label text-white">project=COACH</span>
  </li>
  <li>
    Type of bottle, e.g.
    <span class="badge rounded-pill bg-label text-white">type=model</span>
    <span class="badge rounded-pill bg-label text-white">problem=classification</span>
  </li>
  <li>
    (Hyper)Parameters used to generate the model, e.g.
    <span class="badge rounded-pill bg-label text-white">learning-rate=0.005</span>
    <span class="badge rounded-pill bg-label text-white">layers=7</span>
  </li>
  <li>
    Source code version, e.g.
    <span class="badge rounded-pill bg-label text-white">version=v0.2.4</span>
  </li>
</ul>

<p>
  In short labels are used to identify anything a user might want to use to
  select bottles. The selection of bottles based on labels is done with label
  selectors.
</p>

<p>
  Labels can also be used on parts of bottles. For example a bottle representing
  a data set might have both the training and test sets contained within it as
  different parts. In that case you would want to label them set with something
  like <span class="badge rounded-pill bg-label text-white">subset=train</span> and
  <span class="badge rounded-pill bg-label text-white">subset=test</span>, respectively.
  That way a user can restrict/limit what they download if only a few parts are
  required.
</p>
<h2>Label Selectors</h2>

<p>
  Label selectors (or simply selectors) are used to restrict the set of bottles.
  Assuming users label their bottles appropriately, you can effectively make
  arbitrary groups of bottles by picking which selectors must match.
</p>

<p>
  The syntax used is the same as Kubernetes
  <a
    href="https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/#label-selectors"
    >label selectors</a
  >. Eventually this might be extended to become a superset of what Kubernetes provides.
</p>

<p>
  A selector is composed of zero or more requirements. For example, the selector
  <code>project=COACH,type=model</code> has two requirements
  <code>project=COACH</code> and <code>type=model</code> that must match the
  bottle in order to be selected. All requirements must match for a selector to
  match. So the <code>,</code> in
  <code>project=COACH,type=model</code> semantically means AND. When multiple
  multiple selectors are specified, any one of those selectors must match. So semantically
  selectors are OR&rsquo;d together. In mathematical terms this is disjunctive
  normal form (e.g., (r₀ ∧ r₁ ∧ &hellip;) ∨ (r₃ ∧ &hellip;)). In electrial
  engineering terms this is the &ldquo;sum of products&rdquo; form (i.e., r₀ r₁
  + r₃).
</p>

<p>
  There many ways to specify a requirement. Above we only showed the simplest
  form <code>somekey=somevalue</code>. Note that whitespace within a requirement
  is insignificant. Here are all the available ways to specify a requirement:
</p>

<ol>
  <li>
    <code>key = value</code> and <code>key == value</code> both require that the
    value of <code>key</code> equal <code>value</code>
  </li>
  <li>
    <code>key != value</code> requires that the value of <code>key</code> not be
    <code>value</code>
  </li>
  <li>
    <code>key in (value1, value2)</code> requires that the value of
    <code>key</code> be either <code>value1</code> or <code>value2</code>
  </li>
  <li>
    <code>key notin (value1, value2)</code> requires that the value of
    <code>key</code> be neither <code>value1</code> or <code>value2</code>
  </li>
  <li>
    <code>key</code> requires that a label with <code>key</code> exist (the
    value of <code>key</code> is not checked)
  </li>
  <li>
    <code>!key</code> requires that a label with <code>key</code> not exist (the
    value of <code>key</code> is not checked)
  </li>
  <li>
    <code>key &lt; 5</code> requires that the value of <code>key</code> be less
    than <code>5</code>
  </li>
  <li>
    <code>key &gt; 7</code> requires that the value of <code>key</code> be
    greater than <code>7</code>
  </li>
</ol>

<p>
  In some situations there is a temporary restriction that inequality
  requirements (7 and 8) only work when the value is an integer.
</p>
