## [0.20.1](https://git.act3-ace.com/ace/data/telemetry/compare/v0.20.0...v0.20.1) (2024-05-26)


### Bug Fixes

* lint issues ([9e0ab9a](https://git.act3-ace.com/ace/data/telemetry/commit/9e0ab9a8e5de7eaa56c1038b2b84d70207ab0f42))
* make /tmp writable in the helm chart deployment ([63088f3](https://git.act3-ace.com/ace/data/telemetry/commit/63088f381884c1ed0b762aeeadedf8e5bb4cf212))
* more lint issues in GO ([b2d8062](https://git.act3-ace.com/ace/data/telemetry/commit/b2d8062f834bf1d45b781281600803eb99e4747e))
* user in helm chart did not match container ([9e178ac](https://git.act3-ace.com/ace/data/telemetry/commit/9e178ac9273ad93c5a37c2c8ffb15d99a951ffe3))

# [0.20.0](https://git.act3-ace.com/ace/data/telemetry/compare/v0.19.2...v0.20.0) (2024-05-24)


### Bug Fixes

* 192 Deprecated Bottles window has invisible text ([54022a2](https://git.act3-ace.com/ace/data/telemetry/commit/54022a28385da38411fe188bcf52cf8ac58ffc2a))
* add fips arm64 job ([f39323c](https://git.act3-ace.com/ace/data/telemetry/commit/f39323c8a3986b947a7b5062f203f6a35af0e6cf))
* **deps:** update code.cloudfoundry.org/bytefmt digest to 6038236 ([48de53d](https://git.act3-ace.com/ace/data/telemetry/commit/48de53d5362f5861ccdee836ba02af7509827295))
* **deps:** update dependency devsecops/cicd/pipeline to v19.0.13 ([10f6b59](https://git.act3-ace.com/ace/data/telemetry/commit/10f6b595fb662ecb0e884085832a5efba8fc331a))
* **deps:** update dependency devsecops/cicd/pipeline to v19.0.16 ([ef728f8](https://git.act3-ace.com/ace/data/telemetry/commit/ef728f808014f1ff8584c426d7ccc2cb120bce7f))
* **deps:** update dependency devsecops/cicd/pipeline to v19.0.17 ([d83c41d](https://git.act3-ace.com/ace/data/telemetry/commit/d83c41d7cea988ac2587671ba7d495f5a7fc176c))
* **deps:** update dependency devsecops/cicd/pipeline to v19.0.18 ([6d41406](https://git.act3-ace.com/ace/data/telemetry/commit/6d414060e5f80e7f0cf51cf9d864595e961be849))
* **deps:** update dependency go to v1.22.2 ([8e29d36](https://git.act3-ace.com/ace/data/telemetry/commit/8e29d36c959b5c538ae1dd4f9f0ace8470e5262e))
* **deps:** update dependency nbconvert to v7.16.4 ([7abfca6](https://git.act3-ace.com/ace/data/telemetry/commit/7abfca6387e779d42d1347e4e41e81dd192bd92a))
* **deps:** update docker.io/library/golang docker tag to v1.22.3 ([bc71c65](https://git.act3-ace.com/ace/data/telemetry/commit/bc71c654c5309aea2db8c52601695d19807ce5ae))
* **deps:** update github.com/gomarkdown/markdown digest to 642f0ee ([191ec4e](https://git.act3-ace.com/ace/data/telemetry/commit/191ec4e797212459fcb4dbc3d9951b7861d4291c))
* **deps:** update helm release postgresql to v15.2.9 ([e8f2084](https://git.act3-ace.com/ace/data/telemetry/commit/e8f2084b3e4db66d9e7bee0eceb80b5395d5601c))
* **deps:** update helm release postgresql to v15.3.3 ([329a29c](https://git.act3-ace.com/ace/data/telemetry/commit/329a29cdf543b1e36a1d4575cefa6d210d96756f))
* **deps:** update helm release postgresql to v15.4.0 ([c65b046](https://git.act3-ace.com/ace/data/telemetry/commit/c65b0462457214b7c3528bb42ea73d0c703433d4))
* **deps:** update module code.cloudfoundry.org/bytefmt to v0.0.0-20240522170716-2951b8ebd80e ([40b5826](https://git.act3-ace.com/ace/data/telemetry/commit/40b582608a3f035edd8303431ddd317a4db8c0c7))
* **deps:** update module github.com/prometheus/client_golang to v1.19.1 ([e841b40](https://git.act3-ace.com/ace/data/telemetry/commit/e841b407c51f5922273e14e5f87b9025827331f3))
* **deps:** update module gorm.io/gorm to v1.25.10 ([f16f35a](https://git.act3-ace.com/ace/data/telemetry/commit/f16f35a63eabf7261869110d48ef4c4a1504e689))
* **deps:** update module k8s.io/apimachinery to v0.30.1 ([3c29a22](https://git.act3-ace.com/ace/data/telemetry/commit/3c29a2259a175aa21deb6b86d524dc6041027518))
* jupyter notebook convert viewer ([acb11bf](https://git.act3-ace.com/ace/data/telemetry/commit/acb11bfbe2b5985095d82f10e23c5ed222f6b026))
* leaderboard value column matches current search param ([b8da861](https://git.act3-ace.com/ace/data/telemetry/commit/b8da861d118ecdcc3c572969ac7b15c68ee5b752))
* linting issue SearchByRepository comment ([0732cf8](https://git.act3-ace.com/ace/data/telemetry/commit/0732cf8ebfb8fb86d79bc62961d992da54573884))
* nil pointer panic when search error occurs ([1b9a834](https://git.act3-ace.com/ace/data/telemetry/commit/1b9a834cd9720dd3a835ac6b7451ea645fc4aa92))
* update verify.sh ([7fe1e40](https://git.act3-ace.com/ace/data/telemetry/commit/7fe1e409a01682f72db3fd763dbe265553fda7d7))
* updated signature mediatype for notary ([b66d3cb](https://git.act3-ace.com/ace/data/telemetry/commit/b66d3cbdba0e1c9cc4df7396e4a5951365061098))


### Features

* 193 Metrics should to be displayed in the catalog ([1cd97e0](https://git.act3-ace.com/ace/data/telemetry/commit/1cd97e0953afd6be24f4b80b5497e54638be1f56))
* Add IDs to HTML headers to make them linkable ([6fe67b8](https://git.act3-ace.com/ace/data/telemetry/commit/6fe67b84ef45699d6deea18df56983d8cd83a67e))
* added extraPodLabels value to chart ([a00bc5e](https://git.act3-ace.com/ace/data/telemetry/commit/a00bc5ed68e6e31120959f09c61c90bfc067ca6c))
* Search by bottle location ([be2a328](https://git.act3-ace.com/ace/data/telemetry/commit/be2a3283ef11e35b85bb323e09609e5809a9c430))

## [0.19.2](https://git.act3-ace.com/ace/data/telemetry/compare/v0.19.1...v0.19.2) (2024-04-22)


### Bug Fixes

* Leaderboard metrics optional ([e3e8504](https://git.act3-ace.com/ace/data/telemetry/commit/e3e85049b59d18920b00188b89b7f5137d2d1c0d))

## [0.19.1](https://git.act3-ace.com/ace/data/telemetry/compare/v0.19.0...v0.19.1) (2024-04-19)


### Bug Fixes

* acehub image rollback base image to fix pgadmin pull ([32d7ce4](https://git.act3-ace.com/ace/data/telemetry/commit/32d7ce489a57823dd61887e79b36a9bf1c7b3ea5))

# [0.19.0](https://git.act3-ace.com/ace/data/telemetry/compare/v0.18.7...v0.19.0) (2024-04-19)


### Bug Fixes

* add conditional to required field public key in signature validation ([1562fda](https://git.act3-ace.com/ace/data/telemetry/commit/1562fda091164fa6a80cce499fefcbfefb0bac58))
* add Notary style signature validation to signature processing ([eb8512b](https://git.act3-ace.com/ace/data/telemetry/commit/eb8512b079e8946ca9012f7560a9e0fe692778e3))
* another cogint lint fix attempt ([492a772](https://git.act3-ace.com/ace/data/telemetry/commit/492a772675c8491f782205f42e681990f182ddcb))
* **ci:** add gorelease ([812ea0f](https://git.act3-ace.com/ace/data/telemetry/commit/812ea0fbba7bfd6cf6b0a59338b89568a7db6698))
* **ci:** bump pipeline ([4296a31](https://git.act3-ace.com/ace/data/telemetry/commit/4296a31920b3c3257996894309c573269b438bf6))
* **deps:** update code.cloudfoundry.org/bytefmt digest to 335139c ([110aecf](https://git.act3-ace.com/ace/data/telemetry/commit/110aecf58d5b514cfaef783ce1eeb19d3af4ded7))
* **deps:** update dependencies ([3ba839b](https://git.act3-ace.com/ace/data/telemetry/commit/3ba839b51a3fd48f9ac03a6cebcc77394479382f))
* **deps:** update dependency devsecops/cicd/pipeline to v19.0.11 ([a2532bb](https://git.act3-ace.com/ace/data/telemetry/commit/a2532bbec900577af551f0d1f4862419be69b528))
* **deps:** update dependency devsecops/cicd/pipeline to v19.0.7 ([cc3a8c7](https://git.act3-ace.com/ace/data/telemetry/commit/cc3a8c76142b55631003c2d874a5f29d69972477))
* **deps:** update dependency devsecops/cicd/pipeline to v19.0.9 ([9a8442f](https://git.act3-ace.com/ace/data/telemetry/commit/9a8442fc981e1c8be3062cdc9d542c0567a8dc2a))
* **deps:** update docker.io/library/golang docker tag to v1.22.2 ([0c7d73b](https://git.act3-ace.com/ace/data/telemetry/commit/0c7d73b95b141a27de9dcfac6de30fd39e92f116))
* **deps:** update helm release postgresql to v15.2.4 ([c0bc3ce](https://git.act3-ace.com/ace/data/telemetry/commit/c0bc3cef48744f7afd8467ef8cedd2e9716c7b05))
* **deps:** update helm release postgresql to v15.2.5 ([e43b91c](https://git.act3-ace.com/ace/data/telemetry/commit/e43b91c5527d802706ded397a52689580736e324))
* **deps:** update module git.act3-ace.com/ace/data/schema to v1.2.11 ([76b29dd](https://git.act3-ace.com/ace/data/telemetry/commit/76b29ddc01ae77c1dcef6dcbda2e9e593a7c0e49))
* **deps:** update module k8s.io/apimachinery to v0.30.0 ([f45175d](https://git.act3-ace.com/ace/data/telemetry/commit/f45175d6d0cf825aa10b49e4f96226e90d2f9926))
* **deps:** Updated go and non-go dependencies ([7274e86](https://git.act3-ace.com/ace/data/telemetry/commit/7274e8698169d5365f155fadd2c03a21b235d72e))
* disable gorelease verify ([76e99fa](https://git.act3-ace.com/ace/data/telemetry/commit/76e99fa51edab3aeae302a26ef03436d437d18a7))
* fix paths and bump schema ([04b0b76](https://git.act3-ace.com/ace/data/telemetry/commit/04b0b76f6bd1cd13933cfbdbdc33d3f982bbaf88))
* gorelease script supporting files ([70e8cf2](https://git.act3-ace.com/ace/data/telemetry/commit/70e8cf2870265b9b0b2d6b0fffbbb38d39fee01e))
* gorelease verify ([d3e301c](https://git.act3-ace.com/ace/data/telemetry/commit/d3e301c39f3f228b3791c46db5706b5a26e5724a))
* import cycle and lint errors ([607de9f](https://git.act3-ace.com/ace/data/telemetry/commit/607de9f6f8f15a84f596e3e5b5e4f59ffbcc2726))
* lint issues ([27f2470](https://git.act3-ace.com/ace/data/telemetry/commit/27f24706e34f744faec0384b02bef892506bf907))
* misnamed struct member for parsing subject descriptor ([34f204c](https://git.act3-ace.com/ace/data/telemetry/commit/34f204c88a6c6a2fb2001f09c366676a3d947960))
* move signature validation to client for api consumers to use ([ba77a71](https://git.act3-ace.com/ace/data/telemetry/commit/ba77a713e7107bc946bb03d35c2f9c3f2698f9b9))
* unused log instance ([d0689f6](https://git.act3-ace.com/ace/data/telemetry/commit/d0689f6238622952dcbdd7dcdd1177dc57ab410c))
* update linters ([635c58a](https://git.act3-ace.com/ace/data/telemetry/commit/635c58ad04e49fbb8b1b051f0e60286498f8d95f))


### Features

* infinite catalog scroll ([7e4604d](https://git.act3-ace.com/ace/data/telemetry/commit/7e4604df689fcfd1476b6452d3b2f8559cfaf588))
* signature count on about page ([9126a12](https://git.act3-ace.com/ace/data/telemetry/commit/9126a12434f67e61838b680734ee4ac4287a0add))

## [0.18.7](https://git.act3-ace.com/ace/data/telemetry/compare/v0.18.6...v0.18.7) (2024-03-25)


### Bug Fixes

* **deps:** Postgres 14 update ([ab224a9](https://git.act3-ace.com/ace/data/telemetry/commit/ab224a9a6ce89b48875b683bdd32882e041510b8))

## [0.18.6](https://git.act3-ace.com/ace/data/telemetry/compare/v0.18.5...v0.18.6) (2024-03-25)


### Bug Fixes

* processing manifest includes needed field ([e3e1521](https://git.act3-ace.com/ace/data/telemetry/commit/e3e1521498f438af614032fc5b7982fafc47517c))

## [0.18.5](https://git.act3-ace.com/ace/data/telemetry/compare/v0.18.4...v0.18.5) (2024-03-22)


### Bug Fixes

* catalog links when DefaultBottleSelectors is set ([d031abf](https://git.act3-ace.com/ace/data/telemetry/commit/d031abfc5aeb851cdbdaef4ea992c8cd83c76e68))
* cleanup unused entries in dockerignore ([ef619db](https://git.act3-ace.com/ace/data/telemetry/commit/ef619db3539c3b531d7fdbea7aefbda4b0aa08f0))
* **jq:** corrected the jq filter to work with the new logging tools ([8097e36](https://git.act3-ace.com/ace/data/telemetry/commit/8097e36255294c3d99a56f3023b0630e34c87200))
* Revert "fix(deps): updated postgresql chart to 14.2.3" ([383bbd1](https://git.act3-ace.com/ace/data/telemetry/commit/383bbd10374a4734fef68294f58484d1c4ab839d))
* tune GOMAXPROCS ([fa46896](https://git.act3-ace.com/ace/data/telemetry/commit/fa4689638790ff972d2feed67ba639e54f49967d))

## [0.18.4](https://git.act3-ace.com/ace/data/telemetry/compare/v0.18.3...v0.18.4) (2024-3-20)


### Bug Fixes

* **build:** call `make deps` before building the binary in ci ([7b00517](https://git.act3-ace.com/ace/data/telemetry/commit/7b00517a2448456a7eb1b9ff4af14c0805f923f5))

## [0.18.3](https://git.act3-ace.com/ace/data/telemetry/compare/v0.18.2...v0.18.3) (2024-3-20)


### Bug Fixes

* **build:** prod container image working directory update for proper permissions ([3c4b215](https://git.act3-ace.com/ace/data/telemetry/commit/3c4b215155a3451819262efbca8124ad083b0028))

## [0.18.2](https://git.act3-ace.com/ace/data/telemetry/compare/v0.18.1...v0.18.2) (2024-3-18)


### Bug Fixes

* dockerfile dont copy assets ([433cc86](https://git.act3-ace.com/ace/data/telemetry/commit/433cc86cda9958920a4b2e505eb8198a374ad1b3))

## [0.18.1](https://git.act3-ace.com/ace/data/telemetry/compare/v0.18.0...v0.18.1) (2024-3-18)


### Bug Fixes

* **deps:** updated postgresql chart to 14.2.3 ([ed71fd0](https://git.act3-ace.com/ace/data/telemetry/commit/ed71fd02eb54d405a1215a1119fbbf93c51acb9d))
* **docs:** removed old docs ([eac13c3](https://git.act3-ace.com/ace/data/telemetry/commit/eac13c3061830157969cb9c1f385001a766f9c2c))
* **helm:** fixed empty value for db in chart ([b6234ec](https://git.act3-ace.com/ace/data/telemetry/commit/b6234ec554062fc62b59679f92a0908c4697e5f2))
* **ko:** remove kodata directory since we bake them into the executable now ([9e3cb53](https://git.act3-ace.com/ace/data/telemetry/commit/9e3cb532c032df97f9a576777eecf16e38195d4a))
* prometheus metrics for HTTP duration ([e3cb673](https://git.act3-ace.com/ace/data/telemetry/commit/e3cb67343d86f3293560783ce7e7fc349fefff37))
* remove broken prometheus support from postgres ([c5f06bb](https://git.act3-ace.com/ace/data/telemetry/commit/c5f06bb6f66cf4b4126b2fe56c44599c41fa7e90))

# [0.18.0](https://git.act3-ace.com/ace/data/telemetry/compare/v0.17.0...v0.18.0) (2024-3-14)


### Bug Fixes

* **ci:** bump the pipeline and add a builder for the ipynb image ([9eabcdc](https://git.act3-ace.com/ace/data/telemetry/commit/9eabcdcd13d946de7bfdb3c2c7efc4187f28ba61))
* **dep:** bumped go-common ([01a33f4](https://git.act3-ace.com/ace/data/telemetry/commit/01a33f426d2a489141d81272264fc491682e759d))
* **deps:** bump go-common ([2b9ab3f](https://git.act3-ace.com/ace/data/telemetry/commit/2b9ab3fa68c493b69d227a7a615c878b6d65cd0a))
* **deps:** bump to debian12 and go 1.22 for building ([d57ceed](https://git.act3-ace.com/ace/data/telemetry/commit/d57ceed00e33023ac22450a3e2d6e7b2b7b05835))
* **deps:** bumped pgx version ([8844950](https://git.act3-ace.com/ace/data/telemetry/commit/8844950112686fa4c05a894a46c66a6f6e14e40c))
* **deps:** update postgres docker tag to v16 ([47f5329](https://git.act3-ace.com/ace/data/telemetry/commit/47f5329c73e372618d078442ce6d1e7d7c0f42db))
* **docs:** update markdownlint files ([ecb4c85](https://git.act3-ace.com/ace/data/telemetry/commit/ecb4c852c6ae9fbf7cc2f2ff569a1cd7477e4c9b))
* lower our CVEs to a single medium vulnerability ([5343ef0](https://git.act3-ace.com/ace/data/telemetry/commit/5343ef0e7fc0f636dd924b3afbb308793ceb3c62))
* **pipeline:** fixed embedding docs ([725bc45](https://git.act3-ace.com/ace/data/telemetry/commit/725bc45a65a7153cc6f532ceef8f97505bd2c92e))
* remove obsolete TODO ([7f0f95c](https://git.act3-ace.com/ace/data/telemetry/commit/7f0f95cc18eb645510cc2c3f3b8abc0b22b569ca))
* switch to go-common for running the http server ([f1bdee8](https://git.act3-ace.com/ace/data/telemetry/commit/f1bdee8ba61cea4b5e00cfb013e0ccae15fa13d9))
* update MAINTAINERS file ([b7194bf](https://git.act3-ace.com/ace/data/telemetry/commit/b7194bfd75e55e7cf5c2c918a1761f81f5a4a719))


### Features

* HTMX Search ([71bd871](https://git.act3-ace.com/ace/data/telemetry/commit/71bd871635351b11021d8d51ff08544d8e11f974))
* web assets embedded in binary ([babb98e](https://git.act3-ace.com/ace/data/telemetry/commit/babb98e068710c7c3f18b889e455775af7204c14))

# [0.17.0](https://git.act3-ace.com/ace/data/telemetry/compare/v0.16.3...v0.17.0) (2024-1-16)


### Bug Fixes

* bump dependencies ([4c1cedb](https://git.act3-ace.com/ace/data/telemetry/commit/4c1cedb94a89cfa086e1c3217e66803133d85425))
* bump deps ([aa13e8b](https://git.act3-ace.com/ace/data/telemetry/commit/aa13e8bf462daa81f960e578ef1687f515e2e653))
* bump pipeline to v16 ([f1f97a4](https://git.act3-ace.com/ace/data/telemetry/commit/f1f97a478ec93ab0e7f77bc0004bc763f01fe628))
* bump the pipeline to v15 ([da80b05](https://git.act3-ace.com/ace/data/telemetry/commit/da80b0552111e166e6b23ae7dea893efb3e21a62))
* **ci:** update pipeline version ([607252c](https://git.act3-ace.com/ace/data/telemetry/commit/607252c092bd954da36d954f10f4e168ea6fa322))
* **deps:** update docker images ([2e0b271](https://git.act3-ace.com/ace/data/telemetry/commit/2e0b271c87f770184243b19912a58db85074a354))
* **deps:** update docker.io/library/python docker tag to v3.12 ([a6c131f](https://git.act3-ace.com/ace/data/telemetry/commit/a6c131fcbb3fdbc085f3712a74c6ee59c2922cdc))
* **deps:** upgraded dependencies ([f408de3](https://git.act3-ace.com/ace/data/telemetry/commit/f408de3dee33cfc8607913fc92e32facd89d7f19))
* **docs:** comment out GL issue by email/add Web opt ([9b9ba1b](https://git.act3-ace.com/ace/data/telemetry/commit/9b9ba1be21ba25e959ac0e6490101fc3ed9b25d2))
* testdata cleanup ([a2a318c](https://git.act3-ace.com/ace/data/telemetry/commit/a2a318c5582aa4429a43776408608aea1e618320))


### Features

* Bottle signature support ([dcb82af](https://git.act3-ace.com/ace/data/telemetry/commit/dcb82afac76787b1a135e28d69a24fa22ffcb2a2))
* **ui:** added bottle attribute iconography ([d723e7f](https://git.act3-ace.com/ace/data/telemetry/commit/d723e7f80fd4d278b0375490a3df6a4677c44dcd))
* **ui:** lineage graph improvements using go-echarts ([1da994e](https://git.act3-ace.com/ace/data/telemetry/commit/1da994e74c779416fbad78f0a47b853fdc3575ad))

## [0.16.3](https://git.act3-ace.com/ace/data/telemetry/compare/v0.16.2...v0.16.3) (2023-09-29)


### Bug Fixes

* **ci:** update ci pipeline ([0d3e29b](https://git.act3-ace.com/ace/data/telemetry/commit/0d3e29b845de0ff2af392d9054944d33cfd0ab92))
* **ci:** upgraded the pipelines ([5c2fb40](https://git.act3-ace.com/ace/data/telemetry/commit/5c2fb4080cb7c21fb597140513e2e6982568bcbb))
* govulncheck issue ([32fa41c](https://git.act3-ace.com/ace/data/telemetry/commit/32fa41c163e61f78f21de6db654393dfea0974ac))
* label text on homepage is white on dark background ([a8dff23](https://git.act3-ace.com/ace/data/telemetry/commit/a8dff2325e622ac030e627cb300bebd8b1d9f636))
* **lint:** minor comment additions ([a4bdf32](https://git.act3-ace.com/ace/data/telemetry/commit/a4bdf323e396be9ef767bd3edcb6bead23ed04ff))

## [0.16.2](https://git.act3-ace.com/ace/data/telemetry/compare/v0.16.1...v0.16.2) (2023-07-12)


### Bug Fixes

* **sql:** groupby clause for aggregate function unique pull count ([132da38](https://git.act3-ace.com/ace/data/telemetry/commit/132da38d3540bf824e523321e077a8f32a233334))

## [0.16.1](https://git.act3-ace.com/ace/data/telemetry/compare/v0.16.0...v0.16.1) (2023-06-29)


### Bug Fixes

* postgres fix ([d03d7fb](https://git.act3-ace.com/ace/data/telemetry/commit/d03d7fbdbe0094ebb0903fd77d0225e907c54710))

# [0.16.0](https://git.act3-ace.com/ace/data/telemetry/compare/v0.15.13...v0.16.0) (2023-06-15)


### Bug Fixes

* added yq to the "generate docs" job ([2db0fd0](https://git.act3-ace.com/ace/data/telemetry/commit/2db0fd00c5ed93adcf13d54d8f43b4f915007033))
* and again ([ea1e08d](https://git.act3-ace.com/ace/data/telemetry/commit/ea1e08df7f16b0567131e18d2eafdfbd22f16c0d))
* auth in "generate docs" job was broken ([dd39120](https://git.act3-ace.com/ace/data/telemetry/commit/dd3912047ea5a8617f03e89ddaf2aa1cca45b8a2))
* **ci:** bump CI version ([a94698a](https://git.act3-ace.com/ace/data/telemetry/commit/a94698a6d015db144a88d18785031541b7b2a732))
* **deps:** update dependency devsecops/cicd/pipeline to v9.0.36 ([f79c44d](https://git.act3-ace.com/ace/data/telemetry/commit/f79c44d72a4d1ea0098fe6a0354e30934dfa4c11))
* **deps:** upgraded nbconvert ([9b99782](https://git.act3-ace.com/ace/data/telemetry/commit/9b99782bda05ff4a0f1b1d9715e7a554ea5e71bf))
* **doc:** update config API doc link ([352b23c](https://git.act3-ace.com/ace/data/telemetry/commit/352b23c115c79afc288cee2449772d131de816ad))
* **doc:** update REST API and Web UI links ([b2c9c51](https://git.act3-ace.com/ace/data/telemetry/commit/b2c9c51f955c364652c8279d83cb59860a10bde8))
* improve search errors and results feedback ([e938016](https://git.act3-ace.com/ace/data/telemetry/commit/e9380167c61e6ec3228809d5e2e15777f24a2e0b))
* makefile deps ([7952777](https://git.act3-ace.com/ace/data/telemetry/commit/7952777e82af5491546bcfc42845f97a5b957f12))
* moving swagger out of release.sh ([4404d7d](https://git.act3-ace.com/ace/data/telemetry/commit/4404d7dae95ad5c8f8e934cbe6c9554acbb1d245))
* PATH in job ([9012bcc](https://git.act3-ace.com/ace/data/telemetry/commit/9012bcc2d88b27c61b201df14c23ac1b28c5b991))
* regression in gorm sqlite driver ([40935e7](https://git.act3-ace.com/ace/data/telemetry/commit/40935e7d9bfbd1cb807f7d22f1d44cbf47bc2894))
* release process upgrades to align with ace/data/tool ([7af593f](https://git.act3-ace.com/ace/data/telemetry/commit/7af593f4883d33a8e722fdf50cf3dac8a26ad793))
* trying again ([1e04c91](https://git.act3-ace.com/ace/data/telemetry/commit/1e04c918126a3ad08bb52c882c40072aa4bddf55))
* update deps ([b608b41](https://git.act3-ace.com/ace/data/telemetry/commit/b608b41f6589a14e71d9e69d44690c3cb962620e))
* update pipeline ([50cd2b7](https://git.act3-ace.com/ace/data/telemetry/commit/50cd2b7b48690ad09185791bd09c27c410cab9e1))
* using v4 of yq ([f8cd07d](https://git.act3-ace.com/ace/data/telemetry/commit/f8cd07dcf6e9545f7f107a92320406c2121baf2f))


### Features

* skipInvalid flag added to client uploads ([da5c0c7](https://git.act3-ace.com/ace/data/telemetry/commit/da5c0c7462106d36e70ab6661e040fe600363caf))

## [0.15.13](https://git.act3-ace.com/ace/data/telemetry/compare/v0.15.12...v0.15.13) (2023-03-27)


### Bug Fixes

* Aggregate pull score query ([e4da36b](https://git.act3-ace.com/ace/data/telemetry/commit/e4da36b0514c3d3f060765d85ad00972face8e21))

## [0.15.12](https://git.act3-ace.com/ace/data/telemetry/compare/v0.15.11...v0.15.12) (2023-03-24)


### Bug Fixes

* bump pipeline version ([d8888ae](https://git.act3-ace.com/ace/data/telemetry/commit/d8888aeacee0c3cbd6e74bf00852627175ab17c8))

## [0.15.11](https://git.act3-ace.com/ace/data/telemetry/compare/v0.15.10...v0.15.11) (2023-03-20)


### Bug Fixes

* removed extraneous print ([20f044e](https://git.act3-ace.com/ace/data/telemetry/commit/20f044ecd9c24203be052d1491ffc1c9e3cedb3e))

## [0.15.10](https://git.act3-ace.com/ace/data/telemetry/compare/v0.15.9...v0.15.10) (2023-03-16)


### Bug Fixes

* ace hub image ([dd14ace](https://git.act3-ace.com/ace/data/telemetry/commit/dd14aced7fd747904dadbd2a6037b789a4656100))

## [0.15.9](https://git.act3-ace.com/ace/data/telemetry/compare/v0.15.8...v0.15.9) (2023-03-16)


### Bug Fixes

* bump ci to v9.0.0 ([ab04945](https://git.act3-ace.com/ace/data/telemetry/commit/ab04945512415c0f1080a284159d23842ce6b579))
* **deps:** update dependency devsecops/cicd/pipeline to v9.0.8 ([5398489](https://git.act3-ace.com/ace/data/telemetry/commit/53984893b2cc4de36f77c5cb447aeccbc74f05b7))
* **deps:** update docker.io/busybox docker tag to v1.36 ([2f1a2c4](https://git.act3-ace.com/ace/data/telemetry/commit/2f1a2c466bb69513fbf27ce5eb20b9deb1bfdd2f))
* **deps:** update docker.io/library/python docker tag to v3.11 ([f71a08f](https://git.act3-ace.com/ace/data/telemetry/commit/f71a08f264bb5456001ee1fef0f3a791dba3a47a))
* hover states and other UI issues ([4cf99bb](https://git.act3-ace.com/ace/data/telemetry/commit/4cf99bb76341f446ba67c17c36a50a8a648c9f81))
* release script ([c6437cb](https://git.act3-ace.com/ace/data/telemetry/commit/c6437cb306c0a4dfeb85b2687a27852ed591364e))
* updated schema and fixed apidocs target in Makefile ([1a38a66](https://git.act3-ace.com/ace/data/telemetry/commit/1a38a6605be1596902fc1f4ce47c8d154f5c6008))

## [0.15.8](https://git.act3-ace.com/ace/data/telemetry/compare/v0.15.7...v0.15.8) (2023-01-23)


### Bug Fixes

* label hover ([12ef9ef](https://git.act3-ace.com/ace/data/telemetry/commit/12ef9ef1c85e6e2ba034487a9cf6b4944076d257))

## [0.15.7](https://git.act3-ace.com/ace/data/telemetry/compare/v0.15.6...v0.15.7) (2023-01-04)


### Bug Fixes

* also remove empty part selectors when removing a selector ([c0d98dd](https://git.act3-ace.com/ace/data/telemetry/commit/c0d98dd4ee6d1e06f94ad8f794b142c4f75ba6ed)), closes [#156](https://git.act3-ace.com/ace/data/telemetry/issues/156)
* change pill hover-state color ([69ea7f5](https://git.act3-ace.com/ace/data/telemetry/commit/69ea7f5e05b1d0177cf09ff44f9f8a4e73417cd8))
* text wrapping on the annotations popover ([2eb379e](https://git.act3-ace.com/ace/data/telemetry/commit/2eb379e58f14adf830ac3e9deb28c2f57a182874))
* update the message about using the telemetry server ([51616a7](https://git.act3-ace.com/ace/data/telemetry/commit/51616a7695349631ba455d4e026591212bb89cbc))
* updated dependencies and bumped to the new pipeline ([d263c76](https://git.act3-ace.com/ace/data/telemetry/commit/d263c76870486f3df7eaddf1cc4f1ccd133a429b))
* upgrade go deps and nbconvert ([a0fbeea](https://git.act3-ace.com/ace/data/telemetry/commit/a0fbeea605c2dcffc3b1fc784b73e2a09ca7a6ae))
* upgrade schema and skaffold and ci ([cb734bf](https://git.act3-ace.com/ace/data/telemetry/commit/cb734bf15a973884531fb81f008ab1808f09c9a3))
* upgrade schema to the latest release ([eba4d43](https://git.act3-ace.com/ace/data/telemetry/commit/eba4d4321b38b729e1b7d73109747d48f4793b0b))

## [0.15.6](https://git.act3-ace.com/ace/data/telemetry/compare/v0.15.5...v0.15.6) (2022-12-12)


### Bug Fixes

* bug in selector matching ([4eba345](https://git.act3-ace.com/ace/data/telemetry/commit/4eba34505d4cf4daa1d0405fffec5b15c556f1a2))
* bumped schema again ([1fdc46e](https://git.act3-ace.com/ace/data/telemetry/commit/1fdc46e749a7f5e9fc9c2f95d8f8556527f5d049))

## [0.15.5](https://git.act3-ace.com/ace/data/telemetry/compare/v0.15.4...v0.15.5) (2022-12-12)


### Bug Fixes

* added the "v" prefix to tag in values.yaml ([5525399](https://git.act3-ace.com/ace/data/telemetry/commit/55253993c1bf43e49c0a078fa662e309eb32836e))
* **ci:** bump CI pipeline ([13cdd4a](https://git.act3-ace.com/ace/data/telemetry/commit/13cdd4a24b636b78cbc77fbce40f949095e19892))
* issues w/ color schemes on BootStrap classes ([9a643f2](https://git.act3-ace.com/ace/data/telemetry/commit/9a643f2dfd42c343a56f8a5618d8d6102f735e35))
* minor fix for the makefile ([68ba855](https://git.act3-ace.com/ace/data/telemetry/commit/68ba855a14543c2c4dd3a4cd3bd028194e698fa7))
* removing fs watcher ([53c4dd7](https://git.act3-ace.com/ace/data/telemetry/commit/53c4dd72f5d268ec0dcba5c5155c260f78cb9b1b))
* test script ([2e84471](https://git.act3-ace.com/ace/data/telemetry/commit/2e844716f4ec04b51576be2a93e103fdd2043649))
* Update Styling to Match Ace Hub ([27c98c1](https://git.act3-ace.com/ace/data/telemetry/commit/27c98c19f95dfc1b3678a22cbed005aa0df8fff7))

## [0.15.4](https://git.act3-ace.com/ace/data/telemetry/compare/v0.15.3...v0.15.4) (2022-12-05)


### Bug Fixes

* test script ([925c523](https://git.act3-ace.com/ace/data/telemetry/commit/925c523eff1667c71c796dc33e0ff90ff2ed6066))

## [0.15.3](https://git.act3-ace.com/ace/data/telemetry/compare/v0.15.2...v0.15.3) (2022-12-02)


### Bug Fixes

* **ci:** bump CI pipeline ([1ae17af](https://git.act3-ace.com/ace/data/telemetry/commit/1ae17af7de4098fd684a7de5bc7998c95bf23f70))

## [0.15.2](https://git.act3-ace.com/ace/data/telemetry/compare/v0.15.1...v0.15.2) (2022-12-02)


### Bug Fixes

* minor fix for the makefile ([b65b6bf](https://git.act3-ace.com/ace/data/telemetry/commit/b65b6bf820237cb8b4f992fef5602db39a0850be))

## [0.15.1](https://git.act3-ace.com/ace/data/telemetry/compare/v0.15.0...v0.15.1) (2022-11-24)


### Bug Fixes

* added a test case for v1 bottles ([3bf9675](https://git.act3-ace.com/ace/data/telemetry/commit/3bf96758b08865851a5ed1fb0bd22f8577598d53))
* bumped to support FIPS again ([07ad432](https://git.act3-ace.com/ace/data/telemetry/commit/07ad432df9ffe4cc1186337ac63ab30bcac60813))
* **ci:** bump again ([2c7e546](https://git.act3-ace.com/ace/data/telemetry/commit/2c7e546b30831b85d90df71e8c7bbf52b5770fe0))
* **ci:** fix artifact path ([7d8ab08](https://git.act3-ace.com/ace/data/telemetry/commit/7d8ab08b17eb071b0e8fdf59356c3678f3aa05b6))

# [0.15.0](https://git.act3-ace.com/ace/data/telemetry/compare/v0.14.2...v0.15.0) (2022-11-23)


### Bug Fixes

* bumped schema version ([3675f5a](https://git.act3-ace.com/ace/data/telemetry/commit/3675f5a5c9f7ffb2c638a5c54aaa98a68c9599bd))
* **deps:** update docker.io/library/golang docker tag to v1.19.3 ([2456de4](https://git.act3-ace.com/ace/data/telemetry/commit/2456de482d7c954b9acbc9dabfc55ae1f5c62201))
* **deps:** update helm release postgresql to v11.9.13 ([78584ef](https://git.act3-ace.com/ace/data/telemetry/commit/78584ef86dcfe8c116c2cd08e1e1a67983881600))
* test bottle dependencies ([5b64181](https://git.act3-ace.com/ace/data/telemetry/commit/5b64181796c869adda8360148158883ebd474016))


### Features

* upgrade to bottle v1 ([78084ae](https://git.act3-ace.com/ace/data/telemetry/commit/78084aefb0b0d89436a7dfb62dde88205a244e0b))

## [0.14.2](https://git.act3-ace.com/ace/data/telemetry/compare/v0.14.1...v0.14.2) (2022-11-15)


### Bug Fixes

* typo in README ([21fa6b2](https://git.act3-ace.com/ace/data/telemetry/commit/21fa6b26cdafdd54b878494d1a5b8cbfb5529027))
* updated deps again ([d59e001](https://git.act3-ace.com/ace/data/telemetry/commit/d59e0011dc2e3e397205c8242206f62a7d5451fb))

## [0.14.1](https://git.act3-ace.com/ace/data/telemetry/compare/v0.14.0...v0.14.1) (2022-11-15)


### Bug Fixes

* bump CI pipeline ([997cb80](https://git.act3-ace.com/ace/data/telemetry/commit/997cb80d5bfd6a21b595fd0e6252365352bd3767))
* bump go-common ([b8517f9](https://git.act3-ace.com/ace/data/telemetry/commit/b8517f9e4b5fbd44fdc1d84eca9b3476cfe8e091))
* **deps:** update dependency docker.io/library/golang to v1.19.2 ([eeb77d6](https://git.act3-ace.com/ace/data/telemetry/commit/eeb77d6c025bffed30c00eb6edfe38c029e4cdb1))
* **deps:** update dependency nbconvert to v7.2.1 ([b051628](https://git.act3-ace.com/ace/data/telemetry/commit/b0516283ba020292f5f7ddf9e35f6d8745f530c6))
* **deps:** update dependency postgres to v15 ([ff34855](https://git.act3-ace.com/ace/data/telemetry/commit/ff34855874aecb5123a57190988359a8c16262df))
* **deps:** update helm release postgresql to v11.9.1 ([b0121e6](https://git.act3-ace.com/ace/data/telemetry/commit/b0121e61c80fe1b081781c3595644ab189912531))
* switch to ci-bin ([4bd0d89](https://git.act3-ace.com/ace/data/telemetry/commit/4bd0d89aa881fd5a5a858be10ebae6e9b9102657))

# [0.14.0](https://git.act3-ace.com/ace/data/telemetry/compare/v0.13.4...v0.14.0) (2022-10-24)


### Bug Fixes

* output the jq filter to stdout ([bef0e4b](https://git.act3-ace.com/ace/data/telemetry/commit/bef0e4bbb5641a31ff78820f499e0e9e224d0887))
* upgrade to the latest schema ([880303e](https://git.act3-ace.com/ace/data/telemetry/commit/880303e98d8358e731c5f684ef30d9ff776bcefb))


### Features

* added the filter subcommand ([b8cadfb](https://git.act3-ace.com/ace/data/telemetry/commit/b8cadfb25a463e3451e02067c6bb8d0eab7a3fb2))
* stricter manifest validation ([2a12815](https://git.act3-ace.com/ace/data/telemetry/commit/2a128159dc05b972c059f6487d9766f26e42eb41))

## [0.13.4](https://git.act3-ace.com/ace/data/telemetry/compare/v0.13.3...v0.13.4) (2022-10-12)


### Bug Fixes

* added a BottleDetailURL function ([945b10c](https://git.act3-ace.com/ace/data/telemetry/commit/945b10c76579e1c9ce0cc251ba3589257a90108a))
* bumped bottle processor version ([fa43f24](https://git.act3-ace.com/ace/data/telemetry/commit/fa43f249e2daab2056b566cbf8d07ed9aa3e0a60))

## [0.13.3](https://git.act3-ace.com/ace/data/telemetry/compare/v0.13.2...v0.13.3) (2022-10-03)


### Bug Fixes

* EnvPathOr ([5c8dc05](https://git.act3-ace.com/ace/data/telemetry/commit/5c8dc052ddd50cbd3db86c0e4ed16799d863081d))

## [0.13.2](https://git.act3-ace.com/ace/data/telemetry/compare/v0.13.1...v0.13.2) (2022-10-03)


### Bug Fixes

* added EnvPathOr ([57bb45e](https://git.act3-ace.com/ace/data/telemetry/commit/57bb45e6a06d83b6f33bd28b548e326674158991))

## [0.13.1](https://git.act3-ace.com/ace/data/telemetry/compare/v0.13.0...v0.13.1) (2022-09-30)


### Bug Fixes

* improve auto generated docs ([09fee98](https://git.act3-ace.com/ace/data/telemetry/commit/09fee9864961e2d408b384f672f518161270c346))

# [0.13.0](https://git.act3-ace.com/ace/data/telemetry/compare/v0.12.16...v0.13.0) (2022-09-30)


### Bug Fixes

* error handling ([d7fafc1](https://git.act3-ace.com/ace/data/telemetry/commit/d7fafc12e21a8f847d3b25c9fe454f42d3c598a9)), closes [#141](https://git.act3-ace.com/ace/data/telemetry/issues/141)


### Features

* added a MatchAny function for selectors ([6677a24](https://git.act3-ace.com/ace/data/telemetry/commit/6677a249555838c363470a16b09ad520d0dc5bb3))

## [0.12.16](https://git.act3-ace.com/ace/data/telemetry/compare/v0.12.15...v0.12.16) (2022-09-18)


### Bug Fixes

* improved handling of /api in the URL paths ([109aa1e](https://git.act3-ace.com/ace/data/telemetry/commit/109aa1e6db2ebfac1a30a3f665441ca236389ff3))

## [0.12.15](https://git.act3-ace.com/ace/data/telemetry/compare/v0.12.14...v0.12.15) (2022-09-17)


### Bug Fixes

* added documentation for plogs ([3507516](https://git.act3-ace.com/ace/data/telemetry/commit/350751613431bed10ad49d4ac792b2c2fc8e8442))

## [0.12.14](https://git.act3-ace.com/ace/data/telemetry/compare/v0.12.13...v0.12.14) (2022-09-15)


### Bug Fixes

* **deps:** update helm release postgresql to v11.7.6 ([ad995d7](https://git.act3-ace.com/ace/data/telemetry/commit/ad995d7619531cf707cc6f279e0c90cf889a68df))
* **deps:** update module go.uber.org/zap to v1.23.0 ([a984158](https://git.act3-ace.com/ace/data/telemetry/commit/a984158d6ab9895088d5205dd08f3c8ef43778a8))

## [0.12.13](https://git.act3-ace.com/ace/data/telemetry/compare/v0.12.12...v0.12.13) (2022-08-27)


### Bug Fixes

* added memory to KO ([0ad6880](https://git.act3-ace.com/ace/data/telemetry/commit/0ad68804329412eeed27089e00107189136ebe8c))
* more resources for jobs ([4cd17e0](https://git.act3-ace.com/ace/data/telemetry/commit/4cd17e070a1986096547b279d0fcb77d1ceb82d0))

## [0.12.12](https://git.act3-ace.com/ace/data/telemetry/compare/v0.12.11...v0.12.12) (2022-08-27)


### Bug Fixes

* bumped KO jobs resources ([ec84686](https://git.act3-ace.com/ace/data/telemetry/commit/ec84686e30652850098c9cacdef2962b1eb242d6))

## [0.12.11](https://git.act3-ace.com/ace/data/telemetry/compare/v0.12.10...v0.12.11) (2022-08-26)


### Bug Fixes

* bumped pipelines to hopefully make my pipeline green ([4320bb1](https://git.act3-ace.com/ace/data/telemetry/commit/4320bb19601ab136fdf9f100c45470077cf110da))
* Dockerfile entrypoint was wrong ([ac112e1](https://git.act3-ace.com/ace/data/telemetry/commit/ac112e1bc8bc369c6160c4fbabe737c5313890bc))
* improved version handling based on Justen's feedback ([c85de6d](https://git.act3-ace.com/ace/data/telemetry/commit/c85de6df2ff305887e3154d1b9565715db9aab0b))
* more lint issues ([08579ed](https://git.act3-ace.com/ace/data/telemetry/commit/08579ed78ec362418be1a4470bb843088222a337))
* removed a dead route ([5847da5](https://git.act3-ace.com/ace/data/telemetry/commit/5847da5c39b640a8005d34c1fca52afdd73a45a7))

## [0.12.10](https://git.act3-ace.com/ace/data/telemetry/compare/v0.12.9...v0.12.10) (2022-08-23)


### Bug Fixes

* CI pipelines upgraded to fix buildkit ([e5e8b5b](https://git.act3-ace.com/ace/data/telemetry/commit/e5e8b5b592478d00aa728218e420178212f0c4fd))

## [0.12.9](https://git.act3-ace.com/ace/data/telemetry/compare/v0.12.8...v0.12.9) (2022-08-23)


### Bug Fixes

* version handling when built with "go build" ([3e9cdb5](https://git.act3-ace.com/ace/data/telemetry/commit/3e9cdb58e261330b7db35e95d93b2e979cf27c4f))

## [0.12.8](https://git.act3-ace.com/ace/data/telemetry/compare/v0.12.7...v0.12.8) (2022-08-22)


### Bug Fixes

* bump to go 1.19 ([6e909dd](https://git.act3-ace.com/ace/data/telemetry/commit/6e909dd3d26d7f310e42be8bc649e5832950091f))

## [0.12.7](https://git.act3-ace.com/ace/data/telemetry/compare/v0.12.6...v0.12.7) (2022-08-22)


### Bug Fixes

* ignore .go directory ([36215db](https://git.act3-ace.com/ace/data/telemetry/commit/36215db5beb92c3240361c5708ccc89dcd4819ed))

## [0.12.6](https://git.act3-ace.com/ace/data/telemetry/compare/v0.12.5...v0.12.6) (2022-08-22)


### Bug Fixes

* **deps:** update dependency nbconvert to v6.5.3 ([ed5d4ae](https://git.act3-ace.com/ace/data/telemetry/commit/ed5d4ae5ac471fc5d2b84a439688f5bce1509a97))
* trying to fix version ([029a905](https://git.act3-ace.com/ace/data/telemetry/commit/029a905008ee552f6b651830c892eac3ccc48ab4))

## [0.12.5](https://git.act3-ace.com/ace/data/telemetry/compare/v0.12.4...v0.12.5) (2022-08-19)


### Bug Fixes

* added a comment ([54ae0b2](https://git.act3-ace.com/ace/data/telemetry/commit/54ae0b28c4e5365bc23b1acec2669c71c401cb9e))
* updated the template ([6d0ad0a](https://git.act3-ace.com/ace/data/telemetry/commit/6d0ad0a9f499bb7b684f1a0774c5a13269a8e88c))

## [0.12.4](https://git.act3-ace.com/ace/data/telemetry/compare/v0.12.3...v0.12.4) (2022-08-19)


### Bug Fixes

* added beta and alpha branch support to semantic release ([d21eefe](https://git.act3-ace.com/ace/data/telemetry/commit/d21eefe977a4ec4badc169efc0b7cfdb513fdbcc))

## [0.12.3](https://git.act3-ace.com/ace/data/telemetry/compare/v0.12.2...v0.12.3) (2022-08-19)


### Bug Fixes

* trying again with the pipeline ([987a590](https://git.act3-ace.com/ace/data/telemetry/commit/987a590dbb4115203763dc7cf88b82ee028d9c44))

## [0.12.2](https://git.act3-ace.com/ace/data/telemetry/compare/v0.12.1...v0.12.2) (2022-08-19)


### Bug Fixes

* trying CI again ([a3c9f57](https://git.act3-ace.com/ace/data/telemetry/commit/a3c9f57db8100c627f267778fdffd9e8ce688e20))

## [0.12.1](https://git.act3-ace.com/ace/data/telemetry/compare/v0.12.0...v0.12.1) (2022-08-18)


### Bug Fixes

* **hub:** use ACT3_TOKEN to integrate with the pipeline ([e2a97b7](https://git.act3-ace.com/ace/data/telemetry/commit/e2a97b7663d44578333320b236d6b908c9f9fb84))

# [0.12.0](https://git.act3-ace.com/ace/data/telemetry/compare/v0.11.0...v0.12.0) (2022-08-18)


### Bug Fixes

* **ci:** bumped CI again to fix the chart deps ([4d1c5e4](https://git.act3-ace.com/ace/data/telemetry/commit/4d1c5e47e8091ef2b67a147e24400cd2fea34dcc))
* **ci:** bumped to fix helm chart issues ([43188a4](https://git.act3-ace.com/ace/data/telemetry/commit/43188a4bf40aacceeee26e2b84ff85d29b4b508e))
* **deps:** revert template "go-cli" to v1.0.13 (checkpoint commit made by act3-pt) (go-cli:v1.0.13) ([326302a](https://git.act3-ace.com/ace/data/telemetry/commit/326302ac6ea67e37001311b5a92a9753afec5781))
* **deps:** revert template "go-cli" to v1.0.14 (checkpoint commit made by act3-pt) (go-cli:v1.0.14) ([6a50d0e](https://git.act3-ace.com/ace/data/telemetry/commit/6a50d0e5ffb8ea5be02a874d9948953b85be9e3f))
* **deps:** update dependency devsecops/cicd/pipeline to v7 ([87dab23](https://git.act3-ace.com/ace/data/telemetry/commit/87dab2332033234fe01206434a7c15010a6fdade))
* **deps:** update dependency devsecops/cicd/pipeline to v7.0.4 ([ef1591b](https://git.act3-ace.com/ace/data/telemetry/commit/ef1591bc4f3586e176dad3e7f1dfd907210e20c6))
* **deps:** update dependency devsecops/cicd/pipeline to v7.0.8 ([d4c0d90](https://git.act3-ace.com/ace/data/telemetry/commit/d4c0d90c9fb5a96fc9b0357e9710f08283dee1e8))
* **deps:** update helm release postgresql to v11.6.20 ([d721c8c](https://git.act3-ace.com/ace/data/telemetry/commit/d721c8c203a1479e04c9c4abd5fae437b402f0f4))
* **deps:** update helm release postgresql to v11.6.25 ([724ed17](https://git.act3-ace.com/ace/data/telemetry/commit/724ed17ad90c4c775bc03cfbbf33580b116258a7))
* **deps:** update helm release postgresql to v11.7.1 ([714f546](https://git.act3-ace.com/ace/data/telemetry/commit/714f546df9afbb24815f2b779e518f07c9f5d4a2))
* direct uploading was not working ([e2c69c7](https://git.act3-ace.com/ace/data/telemetry/commit/e2c69c76ef902ccce0341228ee779b9ed0eb4b6b))
* propogate errors ([bfc4255](https://git.act3-ace.com/ace/data/telemetry/commit/bfc4255120fac7bb3ac315ee49bc3235768a7788))
* redaction ([124c659](https://git.act3-ace.com/ace/data/telemetry/commit/124c659215b2a911f65808b0021fd57edb39e916))
* skaffold now work ([151fb22](https://git.act3-ace.com/ace/data/telemetry/commit/151fb22dd6da523a5dca8b5c832f0ecf2f8ebebf))
* switch back to xdg ([3ba1e01](https://git.act3-ace.com/ace/data/telemetry/commit/3ba1e017c35d7b329a2b3069b102809584aee967))


### Features

* upgraded postgres chart ([7949cec](https://git.act3-ace.com/ace/data/telemetry/commit/7949cecbb05e87b36fab8a31fefe1a24c33247f7))

# [0.11.0](https://git.act3-ace.com/ace/data/telemetry/compare/v0.10.14...v0.11.0) (2022-07-11)


### Bug Fixes

* add file ".version.yml" (created by act3-pt) ([5ffb8d3](https://git.act3-ace.com/ace/data/telemetry/commit/5ffb8d366550ef95b4d869f54111583e57d93c64))
* added the api gen docs config file ([aa22daa](https://git.act3-ace.com/ace/data/telemetry/commit/aa22daa67e823a9705cec7c9092bc406c36791fa))
* **deps:** update dependency jinja2 to v3.1.2 ([72c8121](https://git.act3-ace.com/ace/data/telemetry/commit/72c8121b9a96436fb76c8609546e98725d1589b3))
* **deps:** update dependency nbconvert to v6.5.0 ([6657b41](https://git.act3-ace.com/ace/data/telemetry/commit/6657b4121fce23cff570dd5da1c88ec0a2d4353a))
* **deps:** update helm values docker.io/busybox to v1.35 ([1fa0b69](https://git.act3-ace.com/ace/data/telemetry/commit/1fa0b69acb0427fcec5065edfee425d5a682f68b))
* removed old doc approach ([eb12c6d](https://git.act3-ace.com/ace/data/telemetry/commit/eb12c6d49c7851275545b9d0dead9ee7688e8b87))
* we can now output logs to stdout or stderr via a flag ([7e8cd3f](https://git.act3-ace.com/ace/data/telemetry/commit/7e8cd3f4e735859479939409acbf9a1aa3d6c63d))


### Features

* added a new doc generation approach ([0eba7ea](https://git.act3-ace.com/ace/data/telemetry/commit/0eba7eac42cb7d29475a58382f37b7a71a2f3f80))
* added act3-pt ([6662625](https://git.act3-ace.com/ace/data/telemetry/commit/666262559d7304d6b4cf82b37707a78b5756cec1))

## [0.10.14](https://git.act3-ace.com/ace/data/telemetry/compare/v0.10.13...v0.10.14) (2022-07-08)


### Bug Fixes

* expose redaction for the Location struct ([d447094](https://git.act3-ace.com/ace/data/telemetry/commit/d447094dadc150288d6e23a5198ffa6ffa2b5297))

## [0.10.13](https://git.act3-ace.com/ace/data/telemetry/compare/v0.10.12...v0.10.13) (2022-07-08)


### Bug Fixes

* fixed a lint issue and separated out boiler plate code ([a965cbc](https://git.act3-ace.com/ace/data/telemetry/commit/a965cbcbcd963a0d1af848d35a41770475e12845))
* made the redaction code reusable ([9facedc](https://git.act3-ace.com/ace/data/telemetry/commit/9facedc2e84744d0371a99784fb5bbd896eb24a8))
* missed a sample config ([252ac6e](https://git.act3-ace.com/ace/data/telemetry/commit/252ac6ed9a4998bf6481a63365d0b9792b513057))
* removed logging from the telemetry action ([75e39ca](https://git.act3-ace.com/ace/data/telemetry/commit/75e39ca0e49852fa84b5bf98a94a772470ef82f7))
* we are supposed to call sync before we are done with the log in case anything is buffered ([8dc3933](https://git.act3-ace.com/ace/data/telemetry/commit/8dc3933f3d3eae0e4478d9e234918fdec8d4e08f))

## [0.10.12](https://git.act3-ace.com/ace/data/telemetry/compare/v0.10.11...v0.10.12) (2022-06-28)


### Bug Fixes

* updated some comments ([e58737d](https://git.act3-ace.com/ace/data/telemetry/commit/e58737d7bd240888dfd71c693cc3f6e30e2ff850))

## [0.10.11](https://git.act3-ace.com/ace/data/telemetry/compare/v0.10.10...v0.10.11) (2022-06-16)


### Bug Fixes

* updated the docs ([4aed50e](https://git.act3-ace.com/ace/data/telemetry/commit/4aed50e7836fff8e046fbe2348e7b84db5cc674f))

## [0.10.10](https://git.act3-ace.com/ace/data/telemetry/compare/v0.10.9...v0.10.10) (2022-06-16)


### Bug Fixes

* removed version.go ([687db6d](https://git.act3-ace.com/ace/data/telemetry/commit/687db6d2c378850635266a2dafeb9076c316cee0))

## [0.10.9](https://git.act3-ace.com/ace/data/telemetry/compare/v0.10.8...v0.10.9) (2022-06-16)


### Bug Fixes

* added a new source of version information ([338c3c9](https://git.act3-ace.com/ace/data/telemetry/commit/338c3c95fa44402d410efe64580babd256e479e3))
* removed extraneous file ([9e12852](https://git.act3-ace.com/ace/data/telemetry/commit/9e1285292667746b9eff36f57a14541b7300f1e8))

## [0.10.8](https://git.act3-ace.com/ace/data/telemetry/compare/v0.10.7...v0.10.8) (2022-06-16)


### Bug Fixes

* **ci:** update to gitlab 15 ([196e363](https://git.act3-ace.com/ace/data/telemetry/commit/196e3633cc3bae6c39e6da12b681b9c1b84cfcee))
* use filter-coverage.sh in Makefile ([fffad52](https://git.act3-ace.com/ace/data/telemetry/commit/fffad5271b9cd94043128cdcc934d7d3ebabcc65))

## [0.10.7](https://git.act3-ace.com/ace/data/telemetry/compare/v0.10.6...v0.10.7) (2022-06-09)


### Bug Fixes

* **hub:** working to get the dockerfile building again ([0e6131e](https://git.act3-ace.com/ace/data/telemetry/commit/0e6131e185a3f44efde760645368031e405c74c9))

## [0.10.6](https://git.act3-ace.com/ace/data/telemetry/compare/v0.10.5...v0.10.6) (2022-06-09)


### Bug Fixes

* **ci:** get build arg for gitlab credentials ([816a582](https://git.act3-ace.com/ace/data/telemetry/commit/816a5827cdd3fcc7180d2cafbe6f99299012b83c))
* updated act3-dev-tools version ([2e26e8a](https://git.act3-ace.com/ace/data/telemetry/commit/2e26e8afcd2a18c6c73dc22e1e95426de1836345))

## [0.10.5](https://git.act3-ace.com/ace/data/telemetry/compare/v0.10.4...v0.10.5) (2022-06-08)


### Bug Fixes

* moved the interface checking to compile time ([8c8c95a](https://git.act3-ace.com/ace/data/telemetry/commit/8c8c95a847936eb43b6ed0b70fe503471b8ea947))

## [0.10.4](https://git.act3-ace.com/ace/data/telemetry/compare/v0.10.3...v0.10.4) (2022-06-06)


### Bug Fixes

* added back in ipynb without git LFS ([fb123c5](https://git.act3-ace.com/ace/data/telemetry/commit/fb123c5c25ccfed147911c2b083ad51a9cd8655c))
* updated acehub to not use s3.lynx for apt repo ([782ac5a](https://git.act3-ace.com/ace/data/telemetry/commit/782ac5ac862f35f010d57ed23ef188eaa34daccf))

## [0.10.3](https://git.act3-ace.com/ace/data/telemetry/compare/v0.10.2...v0.10.3) (2022-06-06)


### Bug Fixes

* removed act3-pt.yaml until we have it fully setup ([2e5000d](https://git.act3-ace.com/ace/data/telemetry/commit/2e5000d40097fe022fac30e45ec333070c175c7f))

## [0.10.2](https://git.act3-ace.com/ace/data/telemetry/compare/v0.10.1...v0.10.2) (2022-05-27)


### Bug Fixes

* **ci:** added mroe needs statements for the tags.txt ([cd75418](https://git.act3-ace.com/ace/data/telemetry/commit/cd754184c8c541ddb67bb3e2e59046df2a67e2fb))

## [0.10.1](https://git.act3-ace.com/ace/data/telemetry/compare/v0.10.0...v0.10.1) (2022-05-27)


### Bug Fixes

* **ci:** bumped the pipeline to fix a failure ([10b1290](https://git.act3-ace.com/ace/data/telemetry/commit/10b129011841695e9c0b489693722d38885541e4))
* **ci:** ensure the executables are available for the build process ([883a7ce](https://git.act3-ace.com/ace/data/telemetry/commit/883a7cef4a3ba28c8e0765da230cdad8a9e39334))

# [0.10.0](https://git.act3-ace.com/ace/data/telemetry/compare/v0.9.0...v0.10.0) (2022-05-27)


### Bug Fixes

* adding dependency for the linux binary ([6324702](https://git.act3-ace.com/ace/data/telemetry/commit/632470268eb5076e9f9506ee579491a760c21666))
* build issues from the MR ([a914175](https://git.act3-ace.com/ace/data/telemetry/commit/a91417506c0e42c54930db5eb2a583de921fbe49))
* **ci:** fixed unit test job name ([21b7896](https://git.act3-ace.com/ace/data/telemetry/commit/21b789657b321cfde7254c69cc93956f2f14d638))
* trying needs ([ca44021](https://git.act3-ace.com/ace/data/telemetry/commit/ca44021cac30fcdadc90797c677cc9b9648ee496))
* upgraded golangci-lint ([a3f8832](https://git.act3-ace.com/ace/data/telemetry/commit/a3f883272e03a562e7f8b569089f84e9f39d7031))


### Features

* upgraded schema to v1.0.0 to include KRM ([8c53bed](https://git.act3-ace.com/ace/data/telemetry/commit/8c53bedd7ebcbaf4cdda4ed1181cd9e516f288d7))

# [0.9.0](https://git.act3-ace.com/ace/data/telemetry/compare/v0.8.19...v0.9.0) (2022-05-19)


### Bug Fixes

* add file ".version.yml" (created by act3-pt) ([f3bc79b](https://git.act3-ace.com/ace/data/telemetry/commit/f3bc79b8dcaf5dde0f7116d4bcfa4ff3dd51a380))
* add file "config.js" (created by act3-pt) ([c2e8d50](https://git.act3-ace.com/ace/data/telemetry/commit/c2e8d506f3ef99aefe8cc452fe29d45e82994418))
* add file "renovate.json" (created by act3-pt) ([21203ab](https://git.act3-ace.com/ace/data/telemetry/commit/21203ab34acd4c60f7e9826ecb692be69af4a610))
* added back in redaction ([3d678fa](https://git.act3-ace.com/ace/data/telemetry/commit/3d678fa402210d5bf939b154b40f93d424f28bae))
* added validation for descriptors in manifests ([4b9e67f](https://git.act3-ace.com/ace/data/telemetry/commit/4b9e67fb58ce53adbacafac46d624d3c5064e84c))
* bump pipeline version ([b8d671f](https://git.act3-ace.com/ace/data/telemetry/commit/b8d671f387038cba57b3f9b8c14a851a47f9c50b))
* CI build and lint issue ([07bd395](https://git.act3-ace.com/ace/data/telemetry/commit/07bd395ba709eb08932049a824cce9f43f45b03f))
* defaulting for config ([30f7b0d](https://git.act3-ace.com/ace/data/telemetry/commit/30f7b0d79803694ac89558224b046b9598756f7c))
* do not produce duplicate records when re-processing ([80d703c](https://git.act3-ace.com/ace/data/telemetry/commit/80d703cc96a7cd99819eae6f14ef1088195c3a06)), closes [#96](https://git.act3-ace.com/ace/data/telemetry/issues/96)
* lint issue ([ad83e14](https://git.act3-ace.com/ace/data/telemetry/commit/ad83e14720a5a90f5272ca4a24e7d3ca3f2496c5))
* missing authors are fine ([1fff730](https://git.act3-ace.com/ace/data/telemetry/commit/1fff73022e8e2d3aace807b3ed72f64c7f0b5975)), closes [#109](https://git.act3-ace.com/ace/data/telemetry/issues/109)
* removed logger global ([a712f07](https://git.act3-ace.com/ace/data/telemetry/commit/a712f07fb6ef1a5b8377c352047de7b66b885ddd)), closes [#104](https://git.act3-ace.com/ace/data/telemetry/issues/104)
* removed signals (we now import it) ([afdce2b](https://git.act3-ace.com/ace/data/telemetry/commit/afdce2b8ea076b3cafa4e857c3bc42850b6a6cc6))
* semantic-release needs branches now that we are using "main" ([d1b2a79](https://git.act3-ace.com/ace/data/telemetry/commit/d1b2a79cefc7cf4f87a0dd3d175153c9cbbf42bd))
* sort keys in preload to display consistently ([7ec5b24](https://git.act3-ace.com/ace/data/telemetry/commit/7ec5b2475f818c63400f7f368040d54d4901ad16)), closes [#130](https://git.act3-ace.com/ace/data/telemetry/issues/130)
* typo in flag name ([acf0615](https://git.act3-ace.com/ace/data/telemetry/commit/acf06157b6d2d7b85bde6e302b79d879dff21d97))
* unit tests ([ae23e3d](https://git.act3-ace.com/ace/data/telemetry/commit/ae23e3d51c9b093935c36301dfb60ed32861052a))


### Features

* added part selectors ([a5fed5b](https://git.act3-ace.com/ace/data/telemetry/commit/a5fed5b1791dd89047c4079c97eebe8b74ab8c65)), closes [#107](https://git.act3-ace.com/ace/data/telemetry/issues/107)
* YAML view of bottle definition ([7490ee7](https://git.act3-ace.com/ace/data/telemetry/commit/7490ee76e1edf4f4e61a8409e748a953b58e6003)), closes [#108](https://git.act3-ace.com/ace/data/telemetry/issues/108)

## [0.8.19](https://git.act3-ace.com/ace/data/telemetry/compare/v0.8.18...v0.8.19) (2022-04-06)


### Bug Fixes

* name of env for ace-dt ([0942a33](https://git.act3-ace.com/ace/data/telemetry/commit/0942a33e852972ea6053c2b484f8e3d659c31ebe)), closes [#116](https://git.act3-ace.com/ace/data/telemetry/issues/116)

## [0.8.18](https://git.act3-ace.com/ace/data/telemetry/compare/v0.8.17...v0.8.18) (2022-03-28)


### Bug Fixes

* added a ServiceMonitor ([772ad47](https://git.act3-ace.com/ace/data/telemetry/commit/772ad474649ef4034866bd0b35cba673518f604c)), closes [#103](https://git.act3-ace.com/ace/data/telemetry/issues/103)

## [0.8.17](https://git.act3-ace.com/ace/data/telemetry/compare/v0.8.16...v0.8.17) (2022-03-28)


### Bug Fixes

* added commit version to the version string ([59cb170](https://git.act3-ace.com/ace/data/telemetry/commit/59cb17007a54b5a65a5c2c336613b7e781c613ad))

## [0.8.16](https://git.act3-ace.com/ace/data/telemetry/compare/v0.8.15...v0.8.16) (2022-03-28)


### Bug Fixes

* added logging for artifacts ([d872cde](https://git.act3-ace.com/ace/data/telemetry/commit/d872cde1b66242f959b3ee9e7efe5101f3e13d14))
* minor webapp formatting changes ([38a8527](https://git.act3-ace.com/ace/data/telemetry/commit/38a852745f4b387882794e06914af694517feccd))
* nbconvert ([a15695c](https://git.act3-ace.com/ace/data/telemetry/commit/a15695c1378d6b532352f1a72e369f4b00e59344))

## [0.8.15](https://git.act3-ace.com/ace/data/telemetry/compare/v0.8.14...v0.8.15) (2022-03-25)


### Bug Fixes

* added emphasis to make the viewer list ([90f9220](https://git.act3-ace.com/ace/data/telemetry/commit/90f922005356c3ca39dac9c0295ce7710c6a0751))
* bump bottle processor version ([2d55465](https://git.act3-ace.com/ace/data/telemetry/commit/2d55465493ffa0fd1321f5ae613f5ba8b118a5b9))
* removed redundant text in templates and added helper text ([80fcc83](https://git.act3-ace.com/ace/data/telemetry/commit/80fcc8319846e86e2010b3732d11cb92e8bba0d9))

## [0.8.14](https://git.act3-ace.com/ace/data/telemetry/compare/v0.8.13...v0.8.14) (2022-03-24)


### Bug Fixes

* a few more changes of x.bottle ([01ddb7c](https://git.act3-ace.com/ace/data/telemetry/commit/01ddb7c995e7546ecb879f37c2c97b0512aa1c91))

## [0.8.13](https://git.act3-ace.com/ace/data/telemetry/compare/v0.8.12...v0.8.13) (2022-03-24)


### Bug Fixes

* added more environment variables for viewing artifacts in ACE Hub ([052e77e](https://git.act3-ace.com/ace/data/telemetry/commit/052e77eb210f5eb57e4ce288390acbe786afbaf7))

## [0.8.12](https://git.act3-ace.com/ace/data/telemetry/compare/v0.8.11...v0.8.12) (2022-03-23)


### Bug Fixes

* removed FTS from searching for now ([e312410](https://git.act3-ace.com/ace/data/telemetry/commit/e312410a44dda4268631ab23ee7bc863344d9067))

## [0.8.11](https://git.act3-ace.com/ace/data/telemetry/compare/v0.8.10...v0.8.11) (2022-03-23)


### Bug Fixes

* fixed bottle name again to be compatible ([dbf887f](https://git.act3-ace.com/ace/data/telemetry/commit/dbf887ff9020f9b529101ae5515d476631df635c))

## [0.8.10](https://git.act3-ace.com/ace/data/telemetry/compare/v0.8.9...v0.8.10) (2022-03-23)


### Bug Fixes

*  changed the volume name to not conflit with ACE Hub ([f6e5ee0](https://git.act3-ace.com/ace/data/telemetry/commit/f6e5ee011924c11500f1e370e76f63367711cb6c))

## [0.8.9](https://git.act3-ace.com/ace/data/telemetry/compare/v0.8.8...v0.8.9) (2022-03-23)


### Bug Fixes

* chart lint issue ([aeca2e5](https://git.act3-ace.com/ace/data/telemetry/commit/aeca2e59a07ffe73c8e8c7590fe45c58617b3fc5))
* configmap change annotation was missing ([82ed884](https://git.act3-ace.com/ace/data/telemetry/commit/82ed884b821e98e60286b61600730612056c9de0))

## [0.8.8](https://git.act3-ace.com/ace/data/telemetry/compare/v0.8.7...v0.8.8) (2022-03-23)


### Bug Fixes

* markdownlint fixes ([8ca37c3](https://git.act3-ace.com/ace/data/telemetry/commit/8ca37c31e26b961db15b27a5ae47485772b40984))
* nbconvert version ([942fe0f](https://git.act3-ace.com/ace/data/telemetry/commit/942fe0f90b17c5825e12c3dfca4befbb15eceeb9))
* upgrade dockerfiles to GO 1.18 ([112e6b3](https://git.act3-ace.com/ace/data/telemetry/commit/112e6b3d2284c742924c6591ad0668e773399f10))

## [0.8.7](https://git.act3-ace.com/ace/data/telemetry/compare/v0.8.6...v0.8.7) (2022-03-22)


### Bug Fixes

* auth in unit test before_script was missing ([531a622](https://git.act3-ace.com/ace/data/telemetry/commit/531a6221f525d73bbd0172b7adf7a7177adf3416))
* changed some log levels in migration ([53d42bf](https://git.act3-ace.com/ace/data/telemetry/commit/53d42bf31fbb2afb4d5b944800f102e53c5b5c65))
* markdown lint that I missed ([aeb7827](https://git.act3-ace.com/ace/data/telemetry/commit/aeb78273b68a997748e1b5c88ce9aa0e9de26a7f))
* moved aliases next to the digest ([5694eb0](https://git.act3-ace.com/ace/data/telemetry/commit/5694eb0b31007ac8637c41ff0e4d9127d4a17049))
* moved the bottle pulls to the sidebar ([dbc2a40](https://git.act3-ace.com/ace/data/telemetry/commit/dbc2a4013de459db548d41bdf455f71244c6d9ec))
* upgraded the pipeline and the code to GO 1.18 ([6b0e63d](https://git.act3-ace.com/ace/data/telemetry/commit/6b0e63d422a09a7d1a251a612a3c2a481a6b12b0))

## [0.8.6](https://git.act3-ace.com/ace/data/telemetry/compare/v0.8.5...v0.8.6) (2022-03-14)


### Bug Fixes

* changed references from lynx to lion ([f8be6ad](https://git.act3-ace.com/ace/data/telemetry/commit/f8be6ad33f75689d24d8c09d52dcfdc479ec5399))

## [0.8.5](https://git.act3-ace.com/ace/data/telemetry/compare/v0.8.4...v0.8.5) (2022-03-12)


### Bug Fixes

* acehub image ([c284362](https://git.act3-ace.com/ace/data/telemetry/commit/c284362daa9da9108cbca16a2096529383bae460))

## [0.8.4](https://git.act3-ace.com/ace/data/telemetry/compare/v0.8.3...v0.8.4) (2022-03-12)


### Bug Fixes

* made the global config private ([018e6ac](https://git.act3-ace.com/ace/data/telemetry/commit/018e6acb734010b72cde9bfc5dbb39dd265e0300))

## [0.8.3](https://git.act3-ace.com/ace/data/telemetry/compare/v0.8.2...v0.8.3) (2022-03-08)


### Bug Fixes

* build error ([ec1f07e](https://git.act3-ace.com/ace/data/telemetry/commit/ec1f07e877f7319bf663b6f803bc099030282c80))
* jupyter field was not set ([9257a13](https://git.act3-ace.com/ace/data/telemetry/commit/9257a13a053aa7d31bbba5567c24ed577355a386))

## [0.8.2](https://git.act3-ace.com/ace/data/telemetry/compare/v0.8.1...v0.8.2) (2022-02-24)


### Bug Fixes

* added VERSION ([aada01a](https://git.act3-ace.com/ace/data/telemetry/commit/aada01ad7356c5e3660022481ab23e52cece39df))

## [0.8.1](https://git.act3-ace.com/ace/data/telemetry/compare/v0.8.0...v0.8.1) (2022-02-24)


### Bug Fixes

* bake the version into source code ([1449cb4](https://git.act3-ace.com/ace/data/telemetry/commit/1449cb4001d20e624aaae248a9bab82d18428edb))
* updated the docs to reflect the easier install procedure ([72bcdba](https://git.act3-ace.com/ace/data/telemetry/commit/72bcdba93b9023b9be5393d71d7a51b8088822ba))

# [0.8.0](https://git.act3-ace.com/ace/data/telemetry/compare/v0.7.1...v0.8.0) (2022-02-24)


### Bug Fixes

* added a check when uploading to make sure a header is set ([21c7fd6](https://git.act3-ace.com/ace/data/telemetry/commit/21c7fd67fa8a2ba1c359642503471db8e718af88))
* removed replace in go.mod ([e87c515](https://git.act3-ace.com/ace/data/telemetry/commit/e87c5154851accb5eb84b9d9247cf944f4a61adb))


### Features

* Added a search by Author name and email functionality ([c6074c9](https://git.act3-ace.com/ace/data/telemetry/commit/c6074c91023570ecf968323477fe3e2a756382f1))

## [0.7.1](https://git.act3-ace.com/ace/data/telemetry/compare/v0.7.0...v0.7.1) (2022-02-10)


### Bug Fixes

* bumped CI to enable CGO by default ([86cc395](https://git.act3-ace.com/ace/data/telemetry/commit/86cc395897488d114309b48681155a0e04453b29))

# [0.7.0](https://git.act3-ace.com/ace/data/telemetry/compare/v0.6.0...v0.7.0) (2022-02-09)


### Bug Fixes

* moved building out of the dockerfile so we can support private repositories easily. ([10b45b7](https://git.act3-ace.com/ace/data/telemetry/commit/10b45b73c4e579e3c1f0221ca3e0e15c4af37a1a))


### Features

* enabled client-side caching ([33a1854](https://git.act3-ace.com/ace/data/telemetry/commit/33a1854d27a67d101e908f3a1eb9e8de396d8245)), closes [#92](https://git.act3-ace.com/ace/data/telemetry/issues/92)

# [0.6.0](https://git.act3-ace.com/ace/data/telemetry/compare/v0.5.2...v0.6.0) (2022-02-09)


### Bug Fixes

* docker lint issues ([fb3e940](https://git.act3-ace.com/ace/data/telemetry/commit/fb3e940b6813ec6724c75ecaed11a9fdef8eeb0e))
* functional test now builds separate from running the telemetry server to avoid a timeout ([588f340](https://git.act3-ace.com/ace/data/telemetry/commit/588f3404ddcf81b91cdfa700372d83418bab0619))


### Features

* added cookie support to upload and download ([1e0e483](https://git.act3-ace.com/ace/data/telemetry/commit/1e0e4833651f5bb8075fae67233570997400f63d))

## [0.5.2](https://git.act3-ace.com/ace/data/telemetry/compare/v0.5.1...v0.5.2) (2022-01-25)


### Bug Fixes

* run make deps in the Dockerfile ([bfe0980](https://git.act3-ace.com/ace/data/telemetry/commit/bfe098069080647a9a18d212479cfb93f8670b31))

## [0.5.1](https://git.act3-ace.com/ace/data/telemetry/compare/v0.5.0...v0.5.1) (2022-01-25)


### Bug Fixes

* removed debug image ([e12b89f](https://git.act3-ace.com/ace/data/telemetry/commit/e12b89f598bb66802d688ad000c2912d3cfeb784))

# [0.5.0](https://git.act3-ace.com/ace/data/telemetry/compare/v0.4.0...v0.5.0) (2022-01-25)


### Bug Fixes

* **chart:** Set priorityClassName: service-critical as default in the deployment template ([a8ec6f6](https://git.act3-ace.com/ace/data/telemetry/commit/a8ec6f6b46b87bcbdfc7041b9fb6e44a73fd9f4d))
* functional test ([3cbd4df](https://git.act3-ace.com/ace/data/telemetry/commit/3cbd4df10345dc879dde2c34a3858a66699784e4))
* postgres now seems to work ([9d78b71](https://git.act3-ace.com/ace/data/telemetry/commit/9d78b71739234afc63cf97b559ff4acccd825970))


### Features

* added a "from-latest" to better support mirroring of all types ([de94a45](https://git.act3-ace.com/ace/data/telemetry/commit/de94a453b4a30fc649e7af39a4a1b0a05d36eb4c))
* added an about page with a version ([2837fc8](https://git.act3-ace.com/ace/data/telemetry/commit/2837fc8b6ed4d919f040dbd76eb641fffa5e71b8))
* added parent and children of to the leaderboard ([c32ac68](https://git.act3-ace.com/ace/data/telemetry/commit/c32ac68b2150ed134fa2e5c0bf0f962a13c3294a))
* added support for non-bottles and unknown bottles ([3849d41](https://git.act3-ace.com/ace/data/telemetry/commit/3849d4128b24e26b31f9e0a9394835693d7ff235))
* catalog now supports searching for parents and children ([f6ca6ba](https://git.act3-ace.com/ace/data/telemetry/commit/f6ca6ba5e08a7cf0bcbdfa9e6f7ec9e0dc840d8f)), closes [#79](https://git.act3-ace.com/ace/data/telemetry/issues/79)
* initial cut at "Open in ACE Hub" ([4e85678](https://git.act3-ace.com/ace/data/telemetry/commit/4e85678a2f4a914fabc10fe8ce152f19af4cba3c))
* markdown support for artifacts ([442e11f](https://git.act3-ace.com/ace/data/telemetry/commit/442e11fef5ceecf900d8e057a2dee365db8a0b34))

# [0.4.0](https://git.act3-ace.com/ace/data/telemetry/compare/v0.3.2...v0.4.0) (2021-12-23)


### Bug Fixes

* added EXPOSE to dockerfile to support gitlab CI ([9c0ddfc](https://git.act3-ace.com/ace/data/telemetry/commit/9c0ddfc3bf8cfc0d9dd68d76ab55fbd1acaca0b6))


### Features

* added selectors to source references ([dc27a2f](https://git.act3-ace.com/ace/data/telemetry/commit/dc27a2f3a204087dbcf85e806080eba29769b703))

## [0.3.2](https://git.act3-ace.com/ace/data/telemetry/compare/v0.3.1...v0.3.2) (2021-12-22)


### Bug Fixes

* bump CI to fix image digest in the chart ([cc1c041](https://git.act3-ace.com/ace/data/telemetry/commit/cc1c041b5c4115368a193b9463e82b8f9d186b11))

## [0.3.1](https://git.act3-ace.com/ace/data/telemetry/compare/v0.3.0...v0.3.1) (2021-12-21)


### Bug Fixes

* logging bug ([8182f19](https://git.act3-ace.com/ace/data/telemetry/commit/8182f196e2154687970bce8f8f885403d5c0f5fe))
* removed duplicate manifestations ([35bc2da](https://git.act3-ace.com/ace/data/telemetry/commit/35bc2da564d414dd389e4ad94d85031ae8f3eb92))

# [0.3.0](https://git.act3-ace.com/ace/data/telemetry/compare/v0.2.3...v0.3.0) (2021-12-17)


### Bug Fixes

* add more linters ([29d93d0](https://git.act3-ace.com/ace/data/telemetry/commit/29d93d0b298876ca2abcf125a0ebf9a15131741f))
* unit tests ([d72a0ad](https://git.act3-ace.com/ace/data/telemetry/commit/d72a0ad525e4e36664038dd0916de26271c900b4))


### Features

* added a handler for artifacts ([a8efc87](https://git.act3-ace.com/ace/data/telemetry/commit/a8efc87e7e5f9e663a1ecc85a811e9917af57f99)), closes [#18](https://git.act3-ace.com/ace/data/telemetry/issues/18)
* switch to bandwidth in events ([60db710](https://git.act3-ace.com/ace/data/telemetry/commit/60db7102b6d2f00ac845c6f4c9d981c548663684)), closes [#76](https://git.act3-ace.com/ace/data/telemetry/issues/76)

## [0.2.3](https://git.act3-ace.com/ace/data/telemetry/compare/v0.2.2...v0.2.3) (2021-12-16)


### Bug Fixes

* build ([005f70b](https://git.act3-ace.com/ace/data/telemetry/commit/005f70b06b26f541d7574f76d0aa1de5c0e8a628))
* trying --cache=false ([8d4e9db](https://git.act3-ace.com/ace/data/telemetry/commit/8d4e9dbc99e9427b37c9e343f52b74392a490233))
* update CI to fix sub paths ([4382990](https://git.act3-ace.com/ace/data/telemetry/commit/43829907de8908e725a581252f089b73b6b768e3))

## [0.2.2](https://git.act3-ace.com/ace/data/telemetry/compare/v0.2.1...v0.2.2) (2021-12-15)


### Bug Fixes

* add jq highlighting properly ([e6d0b22](https://git.act3-ace.com/ace/data/telemetry/commit/e6d0b22c5621e8ffe6b2fb356ffc5edc48f1beff))
* added a hub target for the Makefile ([4aad639](https://git.act3-ace.com/ace/data/telemetry/commit/4aad639282b172391eb72f6f16afc27397f4ca0f))

## [0.2.1](https://git.act3-ace.com/ace/data/telemetry/compare/v0.2.0...v0.2.1) (2021-12-15)


### Bug Fixes

* CI build ([6129b34](https://git.act3-ace.com/ace/data/telemetry/commit/6129b3481b07e9773bf431cd55ba167f6997eeee))
* lint ([9dbce19](https://git.act3-ace.com/ace/data/telemetry/commit/9dbce1912d197cd17ace97b8d6815bbb97699ecf))

# [0.2.0](https://git.act3-ace.com/ace/data/telemetry/compare/v0.1.4...v0.2.0) (2021-12-15)


### Bug Fixes

* docs ([708f983](https://git.act3-ace.com/ace/data/telemetry/commit/708f983812fa85eefe2336a395eba002707566f0))
* lint by moving validation code out of bottle processor ([45fce14](https://git.act3-ace.com/ace/data/telemetry/commit/45fce14132dabc88fc1b2401a85533703b250759))
* time handling in "additional locations" ([d911fab](https://git.act3-ace.com/ace/data/telemetry/commit/d911fab702bdaa480127db613f7671050d581475))
* unit test ([0fbd7b5](https://git.act3-ace.com/ace/data/telemetry/commit/0fbd7b55ed4b56fd9ee6634bc2dd851cebbc1586))


### Features

* added bottle validation ([1c37fab](https://git.act3-ace.com/ace/data/telemetry/commit/1c37fab0b825d61536a397c8bf0c293accfce8ea))

## [0.1.4](https://git.act3-ace.com/ace/data/telemetry/compare/v0.1.3...v0.1.4) (2021-12-07)


### Bug Fixes

* another postgres fix ([a40992f](https://git.act3-ace.com/ace/data/telemetry/commit/a40992f2debbee73dd55e1eb71d2d3bb576bf15d))
* postgres regression ([ef8be8a](https://git.act3-ace.com/ace/data/telemetry/commit/ef8be8a40309e01de512bf0149de7b6069ccd161))

## [0.1.3](https://git.act3-ace.com/ace/data/telemetry/compare/v0.1.2...v0.1.3) (2021-12-04)


### Bug Fixes

* CLI flag parsing ([6eaebd7](https://git.act3-ace.com/ace/data/telemetry/commit/6eaebd74149027b43f6d067962b61f2ba4422095))
* download ([a1c837b](https://git.act3-ace.com/ace/data/telemetry/commit/a1c837b5164a635097d454c73b443bfd77433946))
* switched to SHA256 for the Canonical digest for now. ([8958a08](https://git.act3-ace.com/ace/data/telemetry/commit/8958a08444d921a8ba00a1ddf7bcf16f579021d7))

## [0.1.2](https://git.act3-ace.com/ace/data/telemetry/compare/v0.1.1...v0.1.2) (2021-11-18)


### Bug Fixes

* coverage ([5fa3a3a](https://git.act3-ace.com/ace/data/telemetry/commit/5fa3a3a9eed9c95308a83f3757a6233dfdb7249c))

## [0.1.1](https://git.act3-ace.com/ace/data/telemetry/compare/v0.1.0...v0.1.1) (2021-11-17)


### Bug Fixes

* for FTS to work with postgres we need a newer version of postgres than the chart. ([5c3d7fb](https://git.act3-ace.com/ace/data/telemetry/commit/5c3d7fb33f5c54a41c6230191c8ea8863524f465))
* location handler ([764b8ce](https://git.act3-ace.com/ace/data/telemetry/commit/764b8ce107b39d95163c52b20f89e8cb4fc899a2))

# [0.1.0](https://git.act3-ace.com/ace/data/telemetry/compare/v0.0.12...v0.1.0) (2021-11-16)


### Bug Fixes

* Alert if clipboard is unavaillable ([9bab758](https://git.act3-ace.com/ace/data/telemetry/commit/9bab7587558194b668ef4e8feb42f76e59a68b07)), closes [#33](https://git.act3-ace.com/ace/data/telemetry/issues/33)
* better error handling ([364e277](https://git.act3-ace.com/ace/data/telemetry/commit/364e277ed626ec7cc77e85ebfbf238a229d0d842))
* bumped CI version ([f344265](https://git.act3-ace.com/ace/data/telemetry/commit/f3442651e75b31d6c9906f4ea15fc5b86bb584d3))
* **chart:** Use docker.io in image reference ([8e2a69f](https://git.act3-ace.com/ace/data/telemetry/commit/8e2a69fedab0cf150a1f29d1402322a6d2600aef))
* readme ([3e749ec](https://git.act3-ace.com/ace/data/telemetry/commit/3e749ec6302442673d6a1d22fdf7838bec6804b6))
* sort order dropdown ([6a3e00f](https://git.act3-ace.com/ace/data/telemetry/commit/6a3e00f52bac7bd546c2153f4173ab032a241936))
* unit tests ([01dbc36](https://git.act3-ace.com/ace/data/telemetry/commit/01dbc36c3cb46104f416d7d7d34dbc4910a0994a))


### Features

* added label selector to search ([7da4f72](https://git.act3-ace.com/ace/data/telemetry/commit/7da4f725d34f8e8d3efac09302684c583ac59144)), closes [#40](https://git.act3-ace.com/ace/data/telemetry/issues/40)
* numeric values for labels ([8ffaf65](https://git.act3-ace.com/ace/data/telemetry/commit/8ffaf658357dc1c4c87206c33c6886743889b3e0)), closes [#30](https://git.act3-ace.com/ace/data/telemetry/issues/30)
* reprocessing of manifests and events ([2081496](https://git.act3-ace.com/ace/data/telemetry/commit/2081496e5c5527383fd5e9921ae21b7be7c836c9)), closes [#25](https://git.act3-ace.com/ace/data/telemetry/issues/25)

## [0.0.12](https://git.act3-ace.com/ace/data/telemetry/compare/v0.0.11...v0.0.12) (2021-11-08)


### Bug Fixes

* added helm dependency back in but removed the repository ([335e1fd](https://git.act3-ace.com/ace/data/telemetry/commit/335e1fd03309fcc3ed9fe752fcb1430b8dec7741))
* made postgres a local chart ([3c051e8](https://git.act3-ace.com/ace/data/telemetry/commit/3c051e8e2748d0f8bf43d09d52a5831e5a05b161))

## [0.0.11](https://git.act3-ace.com/ace/data/telemetry/compare/v0.0.10...v0.0.11) (2021-11-08)


### Bug Fixes

* trying again ([0120833](https://git.act3-ace.com/ace/data/telemetry/commit/0120833e9b48975052682f60c2fdf4c634322157))

## [0.0.10](https://git.act3-ace.com/ace/data/telemetry/compare/v0.0.9...v0.0.10) (2021-11-08)


### Bug Fixes

* ace hub image ([ecfad44](https://git.act3-ace.com/ace/data/telemetry/commit/ecfad442d646e1b1f58efaf65c8fbaf12fddfb78))

## [0.0.9](https://git.act3-ace.com/ace/data/telemetry/compare/v0.0.8...v0.0.9) (2021-11-08)


### Bug Fixes

* trying 1.6.0 again ([857a92e](https://git.act3-ace.com/ace/data/telemetry/commit/857a92e1872e5f1fa84d12c98dbe246b2c71e243))

## [0.0.8](https://git.act3-ace.com/ace/data/telemetry/compare/v0.0.7...v0.0.8) (2021-11-08)


### Bug Fixes

* added webapp unit tests ([60c6479](https://git.act3-ace.com/ace/data/telemetry/commit/60c64799ab6c664a71ba211ad6f0c78c1e664623))
* bumped CI ([92e6c86](https://git.act3-ace.com/ace/data/telemetry/commit/92e6c86b9ea9e175bdaa998fa680c2695ac66c4a))
* context for kaniko ([cf863d0](https://git.act3-ace.com/ace/data/telemetry/commit/cf863d0e2180c9e60f46e9b9e743c9a599802387))

## [0.0.7](https://git.act3-ace.com/ace/data/telemetry/compare/v0.0.6...v0.0.7) (2021-11-07)


### Bug Fixes

* hopefully this works ([d305e49](https://git.act3-ace.com/ace/data/telemetry/commit/d305e49da98a295e9b56692b825b16540e6cb6ef))

## [0.0.6](https://git.act3-ace.com/ace/data/telemetry/compare/v0.0.5...v0.0.6) (2021-11-07)


### Bug Fixes

* more logging to the CI ([6b17842](https://git.act3-ace.com/ace/data/telemetry/commit/6b178420a94be5016eb4c8f1562e26f6c427b91d))

## [0.0.5](https://git.act3-ace.com/ace/data/telemetry/compare/v0.0.4...v0.0.5) (2021-11-07)


### Bug Fixes

* **ci:** trying again ([fec4273](https://git.act3-ace.com/ace/data/telemetry/commit/fec42731b215f1a0e7914f247b8c6f1d8c21eac7))
* improved a unit test ([4fcd4ca](https://git.act3-ace.com/ace/data/telemetry/commit/4fcd4caeaef678c5bed3ab805704e23b3b93d030))

## [0.0.4](https://git.act3-ace.com/ace/data/telemetry/compare/v0.0.3...v0.0.4) (2021-11-07)


### Bug Fixes

* CI ([fa36063](https://git.act3-ace.com/ace/data/telemetry/commit/fa36063ab758a8278d050918d3828943fd29f1a8))

## [0.0.3](https://git.act3-ace.com/ace/data/telemetry/compare/v0.0.2...v0.0.3) (2021-11-07)


### Bug Fixes

* ace hub image ([8b71afd](https://git.act3-ace.com/ace/data/telemetry/commit/8b71afd3b3176d538eaa6ed8585c07f73053e64f))

## [0.0.2](https://git.act3-ace.com/ace/data/telemetry/compare/v0.0.1...v0.0.2) (2021-11-07)


### Bug Fixes

* added version file ([7034dc5](https://git.act3-ace.com/ace/data/telemetry/commit/7034dc5be190a4933ddce1e54d4bfdb2dfecaa6d))
